import toga
from toga.style import Pack
from toga.style.pack import COLUMN, ROW


class Layout_Test(toga.App):
    def startup(self):
        # Create a main window with a name matching the app
        self.main_window = toga.MainWindow(title=self.name)

        main_box = toga.Box()

        first_box = toga.Box(style=Pack(width=200, height = 40))
        first_box.style.update(direction=COLUMN)

        text_input = toga.TextInput()
        multilineTextInput = toga.MultilineTextInput()

        first_box.add(text_input)
        first_box.add(multilineTextInput)

        button = toga.Button("Click Me", style=Pack(width=128, height = 32))

        second_box = toga.Box()
        second_box.add(button)

        main_box.add(first_box)
        main_box.add(second_box)

        # Add the content on the main window
        self.main_window.content = main_box

        # Show the main window
        self.main_window.show()


def main():
    return Layout_Test('layout_test', 'com.gctaa.layout_test')
