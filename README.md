# Installation Notes Debian / Ubuntu

Install the following packages from apt:

    $ sudo apt install git
    $ sudo apt install python3-venv python3-dev
    $ sudo apt install libcairo2-dev libgirepository1.0-dev
    
Create a virtual environment in a designated folder and activate it with the
following set of commands:

    $ python3 -m venv venv
    $ source venv/bin/activate

Install the following libraries using pip:
  
    (venv) $ pip install wheel
    (venv) $ pip install beeware 
    